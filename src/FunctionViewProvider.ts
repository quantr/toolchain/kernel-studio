import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';

export class FunctionViewProvider implements vscode.TreeDataProvider<FunctionNode> {
	constructor(private workspaceRoot: string) { }

	getTreeItem(element: FunctionNode): vscode.TreeItem {
		return element;
	}

	getChildren(element?: FunctionNode): Thenable<FunctionNode[]> {
		if (!element) {
			return Promise.resolve(
				this.getDepsInPackageJson()
			);
		} else {
			return Promise.resolve([]);
		}
	}

	private getDepsInPackageJson(): FunctionNode[] {
		let arr: any[] = [];
		arr.push(new FunctionNode("New Project", vscode.TreeItemCollapsibleState.None, path.join(__filename, '..', '..', 'resources', 'light', 'file.svg'), path.join(__filename, '..', '..', 'resources', 'dark', 'file.svg')));
		arr.push(new FunctionNode("Open Project", vscode.TreeItemCollapsibleState.None, path.join(__filename, '..', '..', 'resources', 'light', 'folder-open.svg'), path.join(__filename, '..', '..', 'resources', 'dark', 'folder-open.svg')));
		arr.push(new FunctionNode("Compile", vscode.TreeItemCollapsibleState.None, path.join(__filename, '..', '..', 'resources', 'light', 'boxes.svg'), path.join(__filename, '..', '..', 'resources', 'dark', 'boxes.svg')));
		arr.push(new FunctionNode("Debug", vscode.TreeItemCollapsibleState.None, path.join(__filename, '..', '..', 'resources', 'light', 'bug.svg'), path.join(__filename, '..', '..', 'resources', 'dark', 'bug.svg')));
		arr.push(new FunctionNode("Config", vscode.TreeItemCollapsibleState.None, path.join(__filename, '..', '..', 'resources', 'light', 'list-alt.svg'), path.join(__filename, '..', '..', 'resources', 'dark', 'list-alt.svg')));

		vscode.commands.registerCommand('New Project', (label) => {
			vscode.window.showInformationMessage(label);
		});
		vscode.commands.registerCommand('Open Project', (label) => {
			vscode.window.showInformationMessage(label);
		});
		vscode.commands.registerCommand('Compile', (label) => {
			vscode.window.showInformationMessage(label);
		});
		vscode.commands.registerCommand('Debug', (label) => {
			vscode.window.showInformationMessage(label);
		});
		vscode.commands.registerCommand('Config', (label) => {
			vscode.window.showInformationMessage(label);
		});

		return arr;
	}

	// private _onDidChangeTreeData: vscode.EventEmitter<FunctionNode | undefined> = new vscode.EventEmitter<FunctionNode | undefined>();
	// readonly onDidChangeTreeData: vscode.Event<FunctionNode | undefined> = this._onDidChangeTreeData.event;

	// refresh(): void {
	// 	this._onDidChangeTreeData.fire();
	// }
}

class FunctionNode extends vscode.TreeItem {
	constructor(
		public readonly label: string,
		public readonly collapsibleState: vscode.TreeItemCollapsibleState,
		public readonly lightIcon: string,
		public readonly darkIcon: string,
	) {
		super(label, collapsibleState);
	}

	command = {
		title: this.label,
		command: this.label,
		tooltip: this.label,
		arguments: [
			this.label,
		]
	};

	get tooltip(): string {
		return `tooltip: ${this.label}`;
	}

	get description(): string {
		return '';
	}

	iconPath = {
		light: this.lightIcon,
		dark: this.darkIcon
	};
}
